Zadání:
Impact of single imputation methods on classification error
Problém chybějících dat je v praxi zcela běžný. Existuje několik efektivních metod, jak chybějící data doplnit nebo je ignorovat. Tato úloha je však netypická tím, že nám nechybí pouze jednotky údajů, ale celé sloupce (1 - (n-1), kdy n je počet příznaků). Uvažujeme případ, kdy jsme pro trénování modelu měli k dispozici kompletní dataset, ale v čase predikce nám některé atributy zcela chybí. Cílem práce je provést sadu experimentů, kdy student vyzkouší různé metody pro doplnění spojitých dat (k-NN, regrese, SOM/jiné NN, Bayesian Networks, C4.5, Random Forest, PCA aj. single (!) imputation metody), resp. rekonstrukci příznaků, a úspěšnost jednotlivých metod následně vyhodnotí (v porovnání s přístupem, kdy chybějící atributy ignorujeme). Během experimentů je nezbytné sledovat změny chyby klasifikátoru (např. MLP) na rekonstruovaných datech.


Zpráva je v souboru report.pdf, naměřené výsledky v souboru results.xlsx.
Všechna data jsou zde v repozitáři ve složce /data.
Celý kód je v souboru main.py, ve kterém lze nalézt i postup spuštění a volbu možností pro jednotlivé datasety.